### Ride-my-way_API-V1 :oncoming_taxi:
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT) [![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](http://makeapullrequest.com) [![Build Status](https://travis-ci.org/kwanj-k/ride-my-way-API-V1.svg?branch=develop)](https://travis-ci.org/kwanj-k/ride-my-way-API-V1) [![Coverage Status](https://coveralls.io/repos/github/kwanj-k/ride-my-way-API-V1/badge.svg?branch=ft-tests-%23158576405)](https://coveralls.io/github/kwanj-k/ride-my-way-API-V1?branch=ft-tests-%23158576405) [![PEP8](https://img.shields.io/badge/code%20style-pep8-green.svg)](https://www.python.org/dev/peps/pep-0008/)


Ride-My-Way is a carpooling application where users can offer rides and other users can accept the rides.

### NOTE
* The project is managed using PivotalTracker board [click here](https://www.pivotaltracker.com/n/projects/2179569)

* To see documentation [click here](https://ridemywayv1.docs.apiary.io/#reference/rides/rides)

* To see API on heroku [click here](https://ridemyway-v1.herokuapp.com)



### Getting Started 

* Clone the repository: 

    ```https://github.com/kwanj-k/ride-my-way-API-V1.git```

* Navigate to the cloned repo. 

### Prerequisites

```
1. Python3
2. Flask
3. Postman
```

### Installation and Usage
After navigating to the cloned repo;

Create a virtualenv and activate it::

    create a directory 
    cd into the directory
    virtualenv -p python3 venv
    source venv/bin activate

Install the dependencies::

    pip install -r requirements.txt 

## Configuration

### Using  .env file
In the same level as the run.py file:

    Create a .env file
    Add the following config to the .env file

    ```
    export FLASK_APP="run.py"
    export FLASK_DEBUG=1
    export APP_SETTINGS="development"
    ```

### Using terminal
You could also run the following in your terminal to configure

    export FLASK_APP="run.py"
    export FLASK_DEBUG=1
    export APP_SETTINGS="development"
    

## Running Tests

### Running tests with coverage
Run:
```
pytest --cov-report term-missing --cov=app
```

### Testing on Postman
Run the development server:
  ```
  $ flask run
  ```

On Post man:
    Navigate to [http://localhost:5000/api/v1](http://localhost:5000/api/v1)

To see data needed to make the requests on postman
navigate to [http://localhost:5000](http://localhost:5000) -to see docs

Test the following endpoints:

| EndPoint                       | Functionality                           |
| -------------------------------|:---------------------------------------:|
| POST     /auth/signup          | Register a user                         |
| POST     /auth/signin          | Login a user                            |
| GET      /rides/               | Get all the ride offers                 |
| GET      /rides/Id/            | Get  a ride by id                       |
| POST     /rides/               | Add a ride offer                        | 
| PUT      /rides/Id/            | Update the information of a ride offer  |
| DELETE   /rides/Id/            | Remove a offer offer                    |
| GET      /rides/id/requets/    | Get the ride requests for a given ride  |
| POST     /rides/id/requets/    | Create a ride request                   |
| DELETE   /requets/id           | Delete a ride request                   |

 
## Authors

* **Kelvin Mwangi** - *Initial work* - [kwanj-k](https://github.com/kwanj-k)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

#### Contribution
Fork the repo, create a PR to this repository's develop.
