"""Tests associated with Rides"""
import unittest
import json
from flask import jsonify

from app.apps import create_app
from app.models import Db

config_name = "testing"
app = create_app(config_name)


class TestRides(unittest.TestCase):
    """
    Ride test class.
    """

    def setUp(self):
        """SetUp to declare data to be used for the tests."""
        app.testing = True
        self.app = app.test_client()

        self.data = {
            "origin": "thika",
            "destination": "by-pass",
            "departure": "19:00",
            "user": "kwanj",
            "driver": "zeus"
        }
        self.data_u = {
            "origin": "kinoo",
            "destination": "kariobangi",
            "departure": "19:00",

        }
        self.all = {
            "origin": "thika1",
            "destination": "by-pass1",
            "departure": "18:00",
            "user": "kwanj1"
        }
        self.no_origin = {
            "destination": "by-pass",
            "departure": "19:00"
        }
        self.no_destination = {
            "origin": "thika",
            "departure": "19:00"
        }
        self.no_departure = {
            "origin": "thika",
            "destination": "by-pass"
        }
        self.empty_origin = {
            "origin": "",
            "destination": "by-pass",
            "departure": "19:00"
        }
        self.empty_destination = {
            "origin": "thika",
            "destination": "",
            "departure": "19:00"
        }
        self.empty_departure = {
            "origin": "thika",
            "destination": "by-pass",
            "departure": ""
        }
        self.inavalid_time = {
            "origin": "thika",
            "destination": "by-pass",
            "departure": "56:98"
        }
    def test_ride_creation(self):
        """Test for the create ride endpoint."""
        res = self.app.post(
            "/api/v1/rides",
            data=json.dumps(self.data),
            content_type='application/json')
        response = json.loads(res.data.decode())
        self.assertEqual(res.status_code, 201)
        self.assertEqual(response["status"], "Success!")
        self.assertEqual(response["message"], "Ride successfully created!")
        Db.clean()

    def test_get_all_rides(self):
        """Test get all ride offers."""
        res = self.app.get(
            '/api/v1/rides',)
        self.assertEqual(res.status_code, 200)

    def test_get_api_docs(self):
        """Test get api docs."""
        res = self.app.get('/')
        self.assertEqual(res.status_code, 200)

    def test_get_ride_by_id(self):
        """Test get ride offer by id."""
        self.app.post(
            "/api/v1/rides",
            data=json.dumps(self.data),
            content_type='application/json',)
        res = self.app.get("/api/v1/rides/1")

        self.assertEqual(res.status_code, 200)

    def test_ride_creation_without_origin(self):
        """Test for the create ride without origin."""
        res = self.app.post(
            "/api/v1/rides",
            data=json.dumps(self.no_origin),
            content_type='application/json',)
        response = json.loads(res.data.decode())
        self.assertEqual(res.status_code, 406)
        self.assertEqual(response["status"], "Failed!")
        self.assertEqual(response["message"], "Please provide an origin")

    def test_ride_creation_without_destination(self):
        """Test for the create ride without destination."""
        res = self.app.post(
            "/api/v1/rides",
            data=json.dumps(self.no_destination),
            content_type='application/json')
        response = json.loads(res.data.decode())
        self.assertEqual(res.status_code, 406)
        self.assertEqual(response["status"], "Failed!")
        self.assertEqual(response["message"], "Please provide a destination")

    def test_ride_creation_without_departure(self):
        """Test for the create ride without departure."""
        res = self.app.post(
            "/api/v1/rides",
            data=json.dumps(self.no_departure),
            content_type='application/json')
        response = json.loads(res.data.decode())
        self.assertEqual(res.status_code, 406)
        self.assertEqual(response["status"], "Failed!")
        self.assertEqual(response["message"],
                         "Please provide a departure time")

    def test_ride_with_empty_origin(self):
        """Test if user can enter empty origin."""
        res = self.app.post(
            "/api/v1/rides",
            data=json.dumps(self.empty_origin),
            content_type='application/json')
        response = json.loads(res.data.decode())
        self.assertEqual(res.status_code, 406)
        self.assertEqual(response["status"], "Failed!")
        self.assertEqual(response["message"], "Origin can not be empty")

    def test_ride_with_empty_destination(self):
        """Test if user can enter empty destination."""
        res = self.app.post(
            "/api/v1/rides",
            data=json.dumps(self.empty_destination),
            content_type='application/json')
        response = json.loads(res.data.decode())
        self.assertEqual(res.status_code, 406)
        self.assertEqual(response["status"], "Failed!")
        self.assertEqual(response["message"], "Destination can not be empty")

    def test_ride_with_empty_departure(self):
        """Test if user can enter empty origin."""
        res = self.app.post(
            "/api/v1/rides",
            data=json.dumps(self.empty_departure),
            content_type='application/json')
        response = json.loads(res.data.decode())
        self.assertEqual(res.status_code, 406)
        self.assertEqual(response["status"], "Failed!")
        self.assertEqual(response["message"], "Departure can not be empty")

    def test_invalid_time_input(self):
        """Test if user can enter an invalid."""
        res = self.app.post(
            "/api/v1/rides",
            data=json.dumps(self.inavalid_time),
            content_type='application/json')
        response = json.loads(res.data.decode())
        self.assertEqual(res.status_code, 406)
        self.assertEqual(response["status"], "Failed!")
        self.assertEqual(response["message"],
                         "Please use valid 24hr clock system")
        Db.clean()

    def test_ride_update(self):
        """Test the meal update endpoint"""
        self.app.post('/api/v1/rides',
                    data=json.dumps(self.data),
                    content_type="application/json")
        res = self.app.put('/api/v1/rides/1',
                            data=json.dumps(self.data),
                            content_type="application/json")
        self.assertEqual(res.status_code, 200)
        Db.clean()

    def test_delete_ride(self):
        """Test if a ride can be deleted."""
        self.app.post(
            "/api/v1/rides",
            data=json.dumps(self.data),
            content_type='application/json')
        deleted = self.app.delete("/api/v1/rides/1")
        self.assertEqual(deleted.status_code, 200)


    def test_create_ride_request(self):
        self.app.post(
            "/api/v1/rides",
            data=json.dumps(self.data),
            content_type='application/json')
        res = self.app.post('/api/v1/rides/1/requests',
                            content_type='application/json')
        self.assertEqual(res.status_code, 201)
        Db.clean()

    def test_delete_ride_request(self):
        """Test if a request can be deleted."""
        delete = self.app.delete("/api/v1/requests/18999")
        self.assertEqual(delete.status_code, 404)
        self.app.post(
            "/api/v1/rides",
            data=json.dumps(self.data),
            content_type='application/json')
        self.app.post('/api/v1/rides/1/requests',
                      data=json.dumps(self.data),
                      content_type='application/json')
        deleted = self.app.delete("/api/v1/requests/1")
        self.assertEqual(deleted.status_code, 200)
        Db.clean()

    def test_get_all_requests_for_specific_ride(self):
        self.app.post(
            "/api/v1/rides",
            data=json.dumps(self.data),
            content_type='application/json')
        res = self.app.get('/api/v1/rides/1/requets')
        self.assertEqual(res.status_code, 404)
        self.app.post('/api/v1/rides/1/requests',
                             content_type='application/json')
        res1 = self.app.get('/api/v1/rides/1/requests',
                             content_type='application/json')
        self.assertEqual(res1.status_code, 200)
        Db.clean()


if __name__ == '__main__':
    unittest.main()
