"""Tests associated with authenitication."""
import unittest
import json

from app.apps import create_app
from app.models import Db
config_name = "testing"
app = create_app(config_name)


class TestAuthenitication(unittest.TestCase):
    """
    Authenitication class to test the login and registration endpoints.
    """

    def setUp(self):
        """SetUp to declare data to be used for the tests."""
        app.testing = True
        self.app = app.test_client()
        self.data = {
            "username": "zeus",
            "email": "athena.kay@gmail.com",
            "password": "kwanjkay"
        }
        self.data_s = {
            "username": "zeus1",
            "email": "athena.kay@gmail.com",
            "password":"kwanjkay"
        }
        self.bad_user_name = {
            "username": "    ",
            "email": "kwanj.kay@gmail.com",
            "password": "kwanjkay"
        }
        self.bad_user_email = {
            "username": "kwanj",
            "email": "cuhi.kwanjfregmail.com",
            "password": "kwanjkay"
        }
        self.bad_user_password = {
            "username": "kwanj",
            "email": "cuhi.kwanj@gmail.com",
            "password": "k   a  y"
        }
        self.no_email = {
            "username": "kwanj",
            "password": "kwanjkay"
        }
        self.no_password = {
            "username": "kwanj",
            "email": "kwanj.kay@gmail.com",
        }
        self.invalid_password = {
            "username": "god",
            "email": "athena.kay@gmail.com",
            "password": "kwahsfavkay"
        }
        self.no_username = {
            "email": "kwanj.kay@gmail.com",
            "password": "kwanjkay"
        }

    def test_signup(self):
        """Test for the signup endpoint."""
        res = self.app.post(
            "/api/v1/auth/signup",
            data=json.dumps(self.data),
            content_type='application/json')
        response = json.loads(res.data.decode())
        self.assertEqual(res.status_code, 201)
        self.assertEqual(response["status"], "Success!")
        self.assertEqual(response["message"], "Account successfully created!")
        Db.clean()

    def test_signin(self):
        """Test for the signin endpoint."""
        self.app.post(
            "/api/v1/auth/signup",
            data=json.dumps(self.data),
            content_type='application/json')
        res = self.app.post(
            "/api/v1/auth/signin",
            data=json.dumps(self.data),
            content_type='application/json')
        response = json.loads(res.data.decode())
        self.assertEqual(res.status_code, 200)
        self.assertEqual(response["status"], "Success!")
        self.assertEqual(response["message"], "Logged in!")
        Db.clean()

    def test_unregistered_signin(self):
        """Test an unregistered signin."""
        res = self.app.post(
            "/api/v1/auth/signin",
            data=json.dumps(self.data),
            content_type='application/json')
        response = json.loads(res.data.decode())
        self.assertEqual(res.status_code, 406)
        self.assertEqual(response["status"], "Failed!")
        self.assertEqual(response["message"],
                         "Account not registered,please sign up")
        Db.clean()

    def test_double_signup(self):
        """Test  if user can register more than once."""
        self.app.post(
            "/api/v1/auth/signup",
            data=json.dumps(self.data),
            content_type='application/json')
        res = self.app.post(
            "/api/v1/auth/signup",
            data=json.dumps(self.data_s),
            content_type='application/json')
        response = json.loads(res.data.decode())
        self.assertEqual(res.status_code, 409)
        self.assertEqual(response["status"], "Failed!")
        self.assertEqual(response["message"], "User already exists!")
        Db.clean()

    def test_bad_email_inputs(self):
        """Test  if user can input an invalid email."""
        res = self.app.post(
            "/api/v1/auth/signup",
            data=json.dumps(self.bad_user_email),
            content_type='application/json')
        res1 = self.app.post(
            "/api/v1/auth/signin",
            data=json.dumps(self.bad_user_email),
            content_type='application/json')
        response = json.loads(res.data.decode())
        response1 = json.loads(res1.data.decode())
        self.assertEqual(res.status_code, 406)
        self.assertEqual(res1.status_code, 406)
        self.assertEqual(response["status"], "Failed!")
        self.assertEqual(response1["status"], "Failed!")
        self.assertEqual(response["message"], "Please enter a valid email")
        self.assertEqual(response1["message"], "Please enter a valid email")
        Db.clean()

    def test_bad_username_inputs(self):
        """Test  if user can input an invalid username."""
        res = self.app.post(
            "/api/v1/auth/signup",
            data=json.dumps(self.bad_user_name),
            content_type='application/json')
        response = json.loads(res.data.decode())
        self.assertEqual(res.status_code, 406)
        self.assertEqual(response["status"], "Failed!")
        self.assertEqual(response["message"], "Username cannot be empty")
        Db.clean()

    def test_bad_password_inputs(self):
        """Test  if user can input an invalid password."""
        res = self.app.post(
            "/api/v1/auth/signup",
            data=json.dumps(self.bad_user_password),
            content_type='application/json')
        response = json.loads(res.data.decode())
        self.assertEqual(res.status_code, 406)
        self.assertEqual(response["status"], "Failed!")
        self.assertEqual(response["message"], "Too short password")
        Db.clean()

    def test_no_email_inputs(self):
        """Test  if user can fail to put in an email."""
        res = self.app.post(
            "/api/v1/auth/signup",
            data=json.dumps(self.no_email),
            content_type='application/json')
        res1 = self.app.post(
            "/api/v1/auth/signin",
            data=json.dumps(self.no_email),
            content_type='application/json')
        response = json.loads(res.data.decode())
        response1 = json.loads(res1.data.decode())
        self.assertEqual(res.status_code, 406)
        self.assertEqual(res1.status_code, 406)
        self.assertEqual(response["status"], "Failed!")
        self.assertEqual(response1["status"], "Failed!")
        self.assertEqual(response["message"], "Please supply email")
        self.assertEqual(response1["message"], "Please supply email")
        Db.clean()

    def test_no_password_inputs(self):
        """Test  if user can fail to put in a password."""
        res = self.app.post(
            "/api/v1/auth/signup",
            data=json.dumps(self.no_password),
            content_type='application/json')
        res1 = self.app.post(
            "/api/v1/auth/signin",
            data=json.dumps(self.no_password),
            content_type='application/json')
        response = json.loads(res.data.decode())
        response1 = json.loads(res1.data.decode())
        self.assertEqual(res.status_code, 406)
        self.assertEqual(res1.status_code, 406)
        self.assertEqual(response["status"], "Failed!")
        self.assertEqual(response1["status"], "Failed!")
        self.assertEqual(response["message"], "Please supply password")
        self.assertEqual(response1["message"], "Please supply password")
        Db.clean()

    def test_invalid_password(self):
        """Test if user can use an invalid password."""
        self.app.post(
            "/api/v1/auth/signup",
            data=json.dumps(self.data),
            content_type='application/json')
        res = self.app.post(
            "/api/v1/auth/signin",
            data=json.dumps(self.invalid_password),
            content_type='application/json')
        response = json.loads(res.data.decode())
        self.assertEqual(res.status_code, 409)
        self.assertEqual(response["status"], "Failed!")
        self.assertEqual(response["message"], "Invalid password!")
        Db.clean()

    def test_no_username(self):
        """Test if user can signup with no username."""
        res = self.app.post(
            "/api/v1/auth/signup",
            data=json.dumps(self.no_username),
            content_type='application/json')
        response = json.loads(res.data.decode())
        self.assertEqual(res.status_code, 406)
        self.assertEqual(response["status"], "Failed!")
        self.assertEqual(response["message"], "Please supply username")
        Db.clean()

if __name__ == '__main__':
    unittest.main()
