'''Validators for user inputs'''
import re

time_re = re.compile(r'^(([01]\d|2[0-3]):([0-5]\d)|24:00)$')

def is_time_format(s):
    """Checks date format and makes sure its 24hr clock system."""
    return bool(time_re.match(s))


def space_stripper(data):
    """ Method to remove white space from inputs."""
    striped = "".join(data.split())
    return striped


def email_validator(email):
    '''Validates user provided email.'''
    if re.match(
            "^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z]{2,4})$",
            email):
        return True


def password_validator(password):
    '''Validates user provided password length and removes white space.'''
    striped = re.sub(r'[\s]+', '', password)
    if len(password) > 6 and striped != '':
        return True


def user_name_validator(data):
    '''Validates user provided username and removes white space.'''
    username = "".join(data.split())
    striped = re.sub(r'[\s]+', '', username)
    if re.match("^[a-zA-Z0-9_]*$", username) and striped != '':
        return True
