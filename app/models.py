"""Models file for different data models objects in the app."""
from flask_bcrypt import Bcrypt
from datetime import datetime


class User(object):
    """A User class to model the user entity."""
    pk = 1

    def __init__(self, username, email, password):
        """Initialize user/account paramaters."""
        self.id = User.pk
        self.username = username
        self.email = email
        self.password = Bcrypt().generate_password_hash(password).decode()
        User.pk += 1

    def password_is_valid(self, password):
        """
        Checks the password against it's hash to validates the user's password.
        """
        return Bcrypt().check_password_hash(self.password, password)

    def json_dump(self):
        """A method to return user information."""
        return dict(
            id=self.id,
            username=self.username,
            email=self.email)


class Request(object):
    """A Request class to define the ride object."""
    pk = 1

    def __init__(self, user):
        """Initialize Ride paramaters."""
        self.id = Request.pk
        self.user = user
        self.ride_id = Ride.pk
        Request.pk += 1

    def json_dump(self):
        """A method to return request information."""
        return dict(
            id=self.id,
            user=self.user
        )


class Ride(object):
    """A Ride class to define the ride object."""
    pk = 1

    def __init__(self, origin, destination, departure, driver):
        """Initialize Ride paramaters."""
        self.origin = origin
        self.destination = destination
        self.departure = departure
        self.driver = driver
        self.requests = [req.json_dump() for req in Db.requests]
        self.created_at = datetime.now()
        self.id = Ride.pk
        Ride.pk += 1

    def json_dump(self):
        """A method to return ride offer information."""
        return dict(
            origin=self.origin,
            destination=self.destination,
            departure=self.departure,
            driver=self.driver,
            requests=self.requests,
            id=self.id)


class Db(object):
    """A mock-db class to simulate how a  database would work."""
    users = []
    rides = []
    requests = []

    @classmethod
    def get_user(cls, email):
        """Method to get user with an email"""
        for user in cls.users:
            if user.email == email:
                return user

    @classmethod
    def get_ride_by_id(cls, id):
        """Method to get a ride by id."""
        for ride in cls.rides:
            if ride.id == id:
                return ride

    @classmethod
    def get_request_by_id(cls, id):
        """Mehod to get a request by id."""
        for request in cls.requests:
            if request.id == id:
                return request

    @classmethod
    def clean(cls):
        """Method to delete the whole database."""
        cls.users = []
        cls.rides = []
