""" App module to bring together the whole app."""
import os
from flask import Flask,send_from_directory
from flask_restful import Api
from flask_jwt_extended import JWTManager
from datetime import timedelta

from config import app_config
from app.views import (
    Signin,
    Signup,
    Rides,
    RidesList,
    Requests,
    Requestid
)
jwt = JWTManager()


def create_app(config_name):
    """Create app function to create/bring together the app."""
    app = Flask(__name__)
    api = Api(app)
    app.config.from_object(app_config[config_name])
    app.url_map.strict_slashes = False
    app.config['JWT_SECRET_KEY'] = 'super-secret-key-that'
    app.config['JWT_ACCESS_TOKEN_EXPIRES'] = timedelta(hours=72)
    jwt.init_app(app)

    @app.route('/')
    def api_docs():
        """ Route to the api docs"""
        return send_from_directory('../api-docs', 'docs.html')

    api.add_resource(Signup, '/api/v1/auth/signup')
    api.add_resource(Signin, '/api/v1/auth/signin')
    api.add_resource(RidesList, '/api/v1/rides')
    api.add_resource(Rides, '/api/v1/rides/<int:id>')
    api.add_resource(Requests, '/api/v1/rides/<int:id>/requests')
    api.add_resource(Requestid, '/api/v1/requests/<int:id>')

    return app
