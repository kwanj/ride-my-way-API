"""File to create all the API endpoints."""
from flask import request, json
from flask_restful import Resource
from flask_jwt_extended import (
    jwt_required,
    create_access_token,
    get_jwt_identity,
    get_current_user)
from app.models import Db, User, Ride, Request
from app.validators import (
    is_time_format,
    space_stripper,
    email_validator,
    password_validator,
    user_name_validator
)


class Signin(Resource):
    """A login resource that provides post method for login."""

    def post(self):
        """Validates and logins the user."""
        json_data = request.get_json(force=True)
        if 'email' not in json_data:
            return {"status": "Failed!",
                    "message": "Please supply email"}, 406
        if not email_validator(json_data['email']):
            return {"status": "Failed!",
                    "message": "Please enter a valid email"}, 406
        if 'password' not in json_data:
            return {"status": "Failed!",
                    "message": "Please supply password"}, 406
        user = Db.get_user(email=json_data['email'])
        if user:
            if user.password_is_valid(json_data['password']):
                access_token = create_access_token(identity=json_data['email'])
                return {"status": "Success!",
                        "message": "Logged in!", "token": access_token}, 200
            return {"status": "Failed!", "message": "Invalid password!"}, 409
        return {"status": "Failed!",
                "message":
                    "Account not registered,please sign up"}, 406


class Signup(Resource):
    """A Signup resource with post method for user registration."""

    def post(self):
        """Validates and registers a user."""
        json_data = request.get_json(force=True)
        if 'username' not in json_data:
            return {"status": "Failed!",
                    "message": "Please supply username"}, 406
        if 'email' not in json_data:
            return {"status": "Failed!",
                    "message": "Please supply email"}, 406
        if 'password' not in json_data:
            return {"status": "Failed!",
                    "message": "Please supply password"}, 406

        user_exists = Db.get_user(email=json_data['email'])
        if user_exists:
            return {"status": "Failed!",
                    "message": "User already exists!"}, 409
        else:
            if not email_validator(json_data['email']):
                return {"status": "Failed!","message": "Please enter a valid email"}, 406
            if not password_validator(space_stripper(json_data['password'])):
                return {"status": "Failed!",
                        "message": "Too short password"}, 406
            if not user_name_validator(json_data['username']):
                return {"status": "Failed!",
                        "message": "Username cannot be empty"}, 406
            user = User(username=json_data['username'],
                        email=json_data['email'],
                        password=json_data['password'])
            Db.users.append(user)
            data = user.json_dump()
            res = "Account successfully created!"
            return {"status": "Success!",
                    "message": res, "user_info": data}, 201


class RidesList(Resource):
    """Ride Resource which does not deal with ids."""

    def get(self):
        """Gets all ride offers."""
        rides = Db.rides
        response = [ride.json_dump() for ride in rides]
        reqs = Db.requests
        if [ride.id for ride in rides] == [req.ride_id for req in reqs]:
            res = [req.json_dump() for req in reqs]
            return {"status": "Success!",
                    "data": response, "requests": res}, 200

    def post(self):
        """Creates a ride offer."""
        json_data = request.get_json(force=True)
        if 'origin' not in json_data:
            return {"status": "Failed!",
                    "message": "Please provide an origin"}, 406
        if 'destination' not in json_data:
            return {"status": "Failed!",
                    "message": "Please provide a destination"}, 406
        if 'departure' not in json_data:
            return {"status": "Failed!",
                    "message": "Please provide a departure time"}, 406
        if space_stripper(json_data['origin']) == '':
            return {"status": "Failed!",
                    "message": "Origin can not be empty"}, 406
        if space_stripper(json_data['departure']) == '':
            return {"status": "Failed!",
                    "message": "Departure can not be empty"}, 406
        if not is_time_format(space_stripper(json_data['departure'])):
            return {"status": "Failed!",
                    "message": "Please use valid 24hr clock system"}, 406
        if space_stripper(json_data['destination']) == '':
            return {"status": "Failed!",
                    "message": "Destination can not be empty"}, 406
        current_user = Db.get_user(email=get_jwt_identity())
        ride = Ride(
            origin=space_stripper(json_data['origin']),
            destination=space_stripper(json_data['destination']),
            departure=space_stripper(json_data['departure']),
            driver=current_user
        )
        Db.rides.append(ride)
        response = json.loads(json.dumps(ride.json_dump()))
        return {"status": "Success!",
                "message": "Ride successfully created!", "data": response}, 201


class Rides(Resource):
    """Ride resource that takes ids."""
    def get(self, id):
        """
        Method gets a ride by id.
        """
        ride = Db.get_ride_by_id(id)
        if not ride:
            return {"message": "Ride id does not exist"}
        response = json.loads(json.dumps(ride.json_dump()))
        return{"status": "success!", "data": response}, 200

    def put(self, id):
        """Method to update a given ride offer."""
        json_data = request.get_json(force=True)
        ride = Db.get_ride_by_id(id)
        if ride:
            if json_data['origin']:
                ride.origin = space_stripper(json_data['origin'])
            if json_data['destination']:
                ride.destination = space_stripper(json_data['destination'])
            if json_data['departure']:
                ride.departure = space_stripper(json_data['departure'])
                response = json.loads(json.dumps(ride.json_dump()))
            return{"status": "success", "data": response}, 200
        return {"status":"Failed!", "message":"Ride id does not exist"}

    def delete(self, id):
        """Method to delete/cancel a ride offer."""
        ride = Db.get_ride_by_id(id)
        if ride:
            Db.rides.remove(ride)
            response = json.loads(json.dumps(ride.json_dump()))
            return {"status": "Deleted!", "data": response}, 200
        return {"message": "Ride id does not exist"}


class Requests(Resource):
    """Requests resoure that goes through the rides url."""
    def post(self, id):
        ride = Db.get_ride_by_id(id)
        current_user = Db.get_user(email=get_jwt_identity())
        if ride:
            r = Request(current_user)
            Db.requests.append(r)
            response = json.loads(json.dumps(ride.json_dump()))
            return {"status": "Success!",
                    "message": "Request successfully sent!",
                    "Ride requested": response, "user": "kwanj"}, 201
        return {"status": "Failed!",
                "message": "Ride id does not exist"}

    def get(self, **kwargs):
        reqs = Db.requests
        response = [reqs.json_dump() for reqs in reqs]
        return {"status": "Success!", "data": response}, 200


class Requestid(Resource):
    """Request resource that goes directly to the requests list."""
    def delete(self, id):
        """Method to delete a given ride request."""
        request = Db.get_request_by_id(id)
        current_user = Db.get_user(email=get_jwt_identity())
        if request:
            if request.user == current_user:
                Db.requests.remove(request)
                response = json.loads(json.dumps(request.json_dump()))
            return {"status": "Deleted!", "data": response}, 200
        return {"message": "Request id does not exist"}, 404
