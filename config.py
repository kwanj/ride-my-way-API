"""Configuration file for the API """
import os


class Config(object):
    """Parent configuration class"""
    DEBUG = False


class DevelopmentConfig(Config):
    """Development configuration"""
    DEBUG = True


class TestingConfig(Config):
    """Testing Configuration"""
    TESTING = True
    DEBUG = True


class StagingConfig(Config):
    """Staging configuration"""
    DEBUG = True


class ProductionConfig(Config):
    """Production Configuration"""
    DEBUG = False
    TESTING = False


app_config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'staging': StagingConfig,
    'production': ProductionConfig,
}
